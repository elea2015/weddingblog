<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="blogList">

			<h1 class="text-center py-5"><?php _e( 'Author: ', 'html5blank' ); echo get_the_author(); ?></h1>

			<?php get_template_part('customloopv2'); ?>

		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>