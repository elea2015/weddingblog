<ul class="cat-list">
    <?php wp_list_categories( array(
        'title_li' => '',
        'order'    => 'ASC',
        'orderby'  => 'name',
        'include'  => $term_ids,
        'exclude'  => array( 2,  1),
        'parent' => 0
    ) ); ?>
</ul>