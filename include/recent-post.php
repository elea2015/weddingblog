<?php 
// Custom WP query recent
$args_recent = array(
	'post_type' => array('post'),
	'posts_per_page' => 5,
	'order' => 'DESC',
);

$recent = new WP_Query( $args_recent );

if( $recent->have_posts()):
    $firstPosts = array();
    while ( $recent->have_posts() ): $recent->the_post(); $a++?>

<?php
//variables
    $title =        get_the_title();
    $image =        get_the_post_thumbnail_url('large');
    $link =         get_the_permalink();
    $thePostId =    get_the_ID();
    $firstPosts[] = $post->ID;
    $author_name = get_field('name');

?>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="blog third">
                <div class="blogImg">
                    <a href="<?php echo $link; ?>"><img src="<?php the_post_thumbnail_url('large'); ?>" alt=""></a>
            </div>
            <div class="blogBody">
                <a href="<?php echo $link; ?>"><h3><?php echo $title;?></h3></a>
                <p><?php echo excerpt(15); ?></p>
                <div class="blogAction">
                    <span><?php the_time('M j, Y'); echo " by "; if($author_name){echo $author_name;}else{the_author();}?></span>
                    <ul class="socialSharePostList list-inline">
                        <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="fb-xfbml-parse-ignore"><i class="fab fa-facebook-square"></i></a></li>
                        <li class="list-inline-item"><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>&source=<?php echo home_url();?>"><i class="fab fa-linkedin"></i></a></li>
                        <li class="list-inline-item"><a href="http://twitter.com/home?status=Currentlyreading <?php echo $link; ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter-square"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </article>
        <!-- start optin card-->
    <?php if($a == 2):?>
        <?php get_template_part('include/optin-blog'); ?>
    <?php endif;?>
<?php 
endwhile;
endif;
wp_reset_postdata();?>