<div class="blog third optin">
    <div class="blogImg">
        <img src="<?php echo get_template_directory_uri(); ?>/img/wh-cta.png" alt="">
    </div>
    <div class="blogBody text-center">
        <h4>#WeddingExperts</h4>
        <p>Wedding Hashers has industry experts who share their tips with you for free! Sign up to have their wedding expertise delivered right to you.</p>
        <div class="ctaRow">
            <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
        </div>
    </div>
</div>