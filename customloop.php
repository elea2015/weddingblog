<?php if (have_posts()): while (have_posts()) : the_post(); $a++ ?>

<?php
//variables
    $title =        get_the_title();
    $image =        get_the_post_thumbnail_url('large');
    $link =         get_the_permalink();
    $thePostId =    get_the_ID(); 
    $author_name = get_field('name');

?>

	    <?php if($a == 1):?>
            <div class="blogContainer">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="blog feature">
                            <div class="blogImg">
                                <a href="<?php echo $link; ?>"><img src="<?php the_post_thumbnail_url('large'); ?>" alt=""></a>
                        </div>
                        <div class="blogBody">
                            <a href="<?php echo $link; ?>"><h3><?php echo $title;?></h3></a>
                            <p><?php echo excerpt(24); ?></p>
                            <div class="blogAction">
                                <span>
                                    <?php the_time('F j, Y'); echo " by ";?>
                                    <?php 
                                        if($author_name){
                                            echo $author_name;
                                        }else{
                                            the_author();
                                        }
                                    ?>
                                </span>
                                <ul class="socialSharePostList list-inline">
                                    <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="fb-xfbml-parse-ignore"><i class="fab fa-facebook-square"></i></a></li>
                                    <li class="list-inline-item"><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>&source=<?php echo home_url();?>"><i class="fab fa-linkedin"></i></a></li>
                                    <li class="list-inline-item"><a href="http://twitter.com/home?status=Currentlyreading <?php echo $link; ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter-square"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </article>
                <div class="blog third optin">
                    <div class="blogImg">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/loulette-featured-min.png" alt="">
                    </div>
                    <div class="blogBody text-center">
                        <h4>Instagram Growth Hacks</h4>
                        <p>Get the latest updates, tips, trends and growth hacks for Instagram delivered straight to your inbox by subscribing below.</p>
                            <form class="formStarted my-2 my-lg-0 getStartedForm mx-auto marketerForm" method="post" action="https://kicksta.co/pricing">
                            <input class="form-control mx-0 emailField" name="email" type="email" placeholder="Your Email" aria-label="Search" value="<?php if($emailField) echo $emailField; ?>">
                            <button class="btn btn-secondary mx-0" type="submit">Get Started</button>
                        </form>
                    </div>
                </div>
            </div>
             <div class="blogConteiner">
                <style>
                #searchFormContainer{
                
                }
                #searchForm input[type="search"]{
                    border-top-right-radius:0;
                    border-bottom-right-radius:0;
                    width:180px;
                }
                @media(min-width:768px){
                    #searchForm input[type="search"]{
                        width: calc(387px - 120px);
                    }
                }
                </style>
                <div id="searchFormContainer" class="py-3">
                    <?php get_template_part('searchform'); ?>
                </div>
            </div>
            <div class="blogContainer">
        <?php else: ?>
                <!-- Blog third -->
                <div class="blog third">
                    <!-- blog thidt card-->
                    <div class="blogImg">
                    <a href="<?php echo $link; ?>">
                        <img src="<?php the_post_thumbnail_url('post-third'); ?>" alt="">
                    </a>
                </div>
                <div class="blogBody">
                    <a href="<?php echo $link; ?>">
                        <h4>
                            <?php echo $title; ?>
                        </h4>
                    </a>
                    <p>
                        <?php echo excerpt(24);?>
                    </p>
                    <div class="blogAction">
                        <span>
                            <?php the_time('F j, Y'); echo " by "; the_author(); ?>
                        </span>
                        <ul class="socialSharePostList list-inline">
                            <li class="list-inline-item">
                                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="fb-xfbml-parse-ignore">
                                    <i class="fab fa-facebook-square"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>&source=<?php echo home_url();?>">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="http://twitter.com/home?status=Currentlyreading <?php echo $link; ?>" title="Click to share this post on Twitter">
                                    <i class="fab fa-twitter-square"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
		<!-- end blog thidt card-->
		<?php endif;?>
<?php endwhile; ?>
<?php endif; ?>
