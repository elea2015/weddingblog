$(document).ready(function () {
    //$(".socialSharePostList").hide();
    // $('#menu-item-6215 a').click(function(event){
    //     event.preventDefault();
    //     $('#searchFormContainer').slideToggle(100);
    // });
    $('.blogAction a.socialSharePost').click(function (event) {
        event.preventDefault();
        var shareContainer = $(this).parent();
        var shareList = $(shareContainer).find('.socialSharePostList');
        $(this).hide();
        $(shareList).show();
    });

    // Email form validation
    $('.getStartedForm .btn').click(function (event) {
        var form = $(this).parents('form:first') // Get Button Form Parent
        var emailField = $(form).find('.emailField') // Get email field
        if ($(emailField).val() == '') { // Field validation
            $(form).addClass('animated shake') // Adds shake animation
            $(emailField).addClass('redError') // Adds Red border
            // When animation stops removed animations classes
            $(form).one('webkitAnimationEnd` mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
                $(this).removeClass('animated shake')
                $(emailField).removeClass('redError')
            })
            event.preventDefault()
        }3
    }); // ends email field empty

    
    $('#btnValidate').click(function(event) {
        var sEmail = $('#txtEmail').val();
        if ($.trim(sEmail).length == 0) {
            $('#txtEmail').addClass('redError');
            event.preventDefault();
        }
        if (validateEmail(sEmail)) {
            event.preventDefault();
        }
        else {
            $('#txtEmail').addClass('redError');
        }
    });

});


function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}