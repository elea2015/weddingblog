<?php get_header(); ?>

	<main role="main">
		<!-- section -->
		<section class="blogList">

			<h2 class="text-center py-5"><?php _e( 'Archives', 'html5blank' ); ?></h2>

			<?php get_template_part('customloop'); ?>

		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
