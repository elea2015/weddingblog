<div class='ctaJoin container'>
<style>
    .automaticCTA{
        display:none;
    }
</style>
    <div class='ctaRow'>
        <ul class='list-inline'>
            <li class='list-inline-item emoji'>🤩</li>
            <li class='list-inline-item emoji'>🤙</li>
            <li class='list-inline-item emoji'>🤑</li>
        </ul>
        <h2><?php block_field( 'heading' ); ?></h2>
        <p><?php block_field( 'sub-header' ); ?></p>
        <div class="ctaRow">
            <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
        </div>
    </div>
</div>