<div class="order-cta">
    <h5><?php block_field( 'heading' ); ?></h5>
    <a class="btn btn-primary" href="https://weddinghashers.com/#hashtag-step-1">Order now</a>
</div>