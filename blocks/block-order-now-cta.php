<style>
    .new-order-cta{
        display:flex;
        align-items:center;
        background-image: url("<?php echo get_template_directory_uri()?>/img/cta-bg-2.jpg");
        background-size:cover;
    }
    .new-order-cta div:first-child{
        width:50%;
        padding: 34px 27px 34px 0px;
        text-align:center;    
    }

    .new-order-cta div:first-child img{
        max-width:100px;
    }

    .new-order-cta div:last-child{
        width:50%;
    }
    .new-order-cta div:last-child img{
        max-width: 43%;
        margin: 0 24px 0 auto;
        display: block;
    }

    .new-order-cta .btn{
        line-height:26px !important;
        font-family: 'Cormorant', serif;
        font-size: 20px !important;
        font-weight:500;
        background:#F2DDD7;
        color: #835858;
        font-weight:bold;
        text-transform:capitalize;
    }

    .new-order-cta .btn:hover{
        color:#F2DDD7;
        background: #835858;
    }

    .new-order-cta h5{
        font-family: 'Cormorant', serif;
        font-weight:bold;
        font-size:23px;
        margin-top:18px;
        color:white;
    }

    @media(max-width:768px){
        .new-order-cta{
            display: initial;
        }

        .new-order-cta div:last-child{
            display:none;
        }
        
        .new-order-cta div:first-child{
            width:100%;
            padding:160px 58px 10px;
            background-image: url("<?php echo get_template_directory_uri()?>/img/cta-bg-2.jpg");
            background-size:cover;
            background-position:center;
            
        }

        .new-order-cta h5{
            margin-bottom:7px;
            color: #835858;
        }
    }
</style>

<div class="new-order-cta">
    <div>
        <h5><?php block_field( 'heading' ); ?></h5>
        <a class="btn btn-primary" href="https://weddinghashers.com/#hashtag-step-1">Order now</a>
    </div>
    <div>
        <img src="<?php echo get_template_directory_uri()?>/img/new-logo.svg" alt="Wedding Hashers Logo">
    </div>
</div>