<div class="order-cta">
    <div><img src="<?php echo get_template_directory_uri()?>/img/logo-symbol.svg" alt="Wedding Hashers Logo"></div>
    <div>
        <h5><?php block_field( 'heading' ); ?></h5>
        <a class="btn btn-primary" href="https://weddinghashers.com/#hashtag-step-1">Order now</a>
    </div>
</div>