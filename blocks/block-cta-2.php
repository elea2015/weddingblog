<div class="ctaJoin ctaSubscribe ctaSubscribeP">
    <div class="ctaRow">
        <p>Wedding Hashers has industry experts who share their tips with you for free! Sign up to have their wedding expertise delivered right to you.</p>
        <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
    </div>
</div>