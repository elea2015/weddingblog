<style>
    .gift-cta{
        background: url('<?php echo get_template_directory_uri()?>/img/cta-bg-3.jpg') !important;
        background-size:cover !important;
        flex-direction: row-reverse;
        padding:2% !important;
        justify-content:space-between;
    }
    .gift-cta div:last-child{
        text-align:center;
        width:50% !important ;
    }

    .gift-cta div:last-child h5{
        font-size:22px !important;
        margin:0 !important;
    }

    .gift-cta div:last-child p{
        color: #835858;
        font-size:18px;
        width: 80%;
        line-height: 1.4;
        margin: 10px auto;
    }

    @media (max-width:768px) {
        .gift-cta{
            flex-direction: column;
            padding: 30px 10px !important;
        }
        .gift-cta div:last-child{
            width:100% !important;
        }
    }
</style>

<div class="order-cta gift-cta">
    <div><img src="<?php echo get_template_directory_uri()?>/img/logo-symbol.svg" alt="Wedding Hashers Logo"></div>
    <div>
        <h5><?php block_field( 'title' ); ?></h5>
        <p><?php block_field( 'text' ); ?></p>
        <a class="btn btn-primary" href="<?php block_field( 'link' ); ?>"><?php block_field( 'button' ); ?></a>
    </div>
</div>