<?php get_header(); 
$author_image = get_field('profile_pic');
$author_name = get_field('name');
?>

	
<section class="caseSingle">
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<!-- article -->
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	    $u_time = get_the_time('U'); 
        $u_modified_time = get_the_modified_time('U'); 
        if ($u_modified_time >= $u_time + 86400) { 
            $update = get_the_modified_time('M j, Y'); 
        } 
	?>
		<div class="">
			<?php if ( !is_singular( 'quiz' ) ):?>
		    <h1 class="text-center"><?php the_title();?></h1>
			<!-- <p class="text-center"><?php //echo do_shortcode('[rt_reading_time label="Reading Time:" postfix="minutes" postfix_singular="minute"]');?></p> -->
			<div class="caseImg text-center">
				<?php the_post_thumbnail('large'); // Fullsize image for the single post ?>
			</div>
			
			<div class="single-meta-cta">
				<div class="author-meta-box">
					<div class="author-img">
						<?php 
							if($author_image){
								echo wp_get_attachment_image( $author_image, 'author' );
							}else{
								echo get_avatar( get_the_author_meta( 'ID' ), 96 ); 
							}
						?>
					</div>
					<div class="author-info">
						<p class="caseDate">
							<?php 
								if($author_name){
									echo $author_name;
								}else{
									the_author();
								}
							?>
						</p>
						<p class="caseDate">Published: <?php the_time('M j, Y');?></p>
						<?php if($update): ?> 
							<p class="caseDate">Last Updated: <?php echo $update ?></p>
						<?php endif;?>
					</div>
				</div>

				<div class="single-cta">
					<h5>Get The Best Wedding Hashtags From Professional Writers</h5>
					<a class="btn btn-primary" href="https://weddinghashers.com/#hashtag-step-1">Order now</a>
				</div>
			</div>
			<?php 
			endif;
			the_content();?>
		</div>
        <div class="ctaJoin ctaSubscribe ctaSubscribeP">
            <div class="ctaRow">
                <h2>#WeddingExperts</h2>
                <p>Wedding Hashers has industry experts who share their tips with you for free! Sign up to have their wedding expertise delivered right to you.</p>
                <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]')?>
            </div>
        </div>
	<?php comments_template();?>
	</article>
	<?php endwhile; ?>
	<?php endif; ?>
</section>

<?php if ( !is_singular( 'quiz' ) ):?>
<section class="blogList">
	<h2 class="text-center mb-5">Related Posts</h2>
	<div class="blogContainer">
		<?php
		  $orig_post = $post;
		  global $post;
		  $categories = get_the_category($post->ID);
		   
		  if ($categories) {
		  $category_ids = array();
			foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
			$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> 3, // Number of related posts that will be shown.
				'caller_get_posts'=>1
			);
		   
		  $my_query = new wp_query( $args );
		 
		  while( $my_query->have_posts() ) {
		  $my_query->the_post();

		  $title = get_the_title();
		  $link = get_the_permalink();
		  $author_name = get_field('name');
		  ?>
		   
			  <!-- Blog third -->
		  
		    <div class="blog third">
		        <!-- blog thidt card-->
		        <div class="blogImg">
		            <a href="<?php echo $link; ?>"><img src="<?php the_post_thumbnail_url('post-third'); ?>" alt=""></a>
		    	</div>
		        <div class="blogBody" style="background: #fff;">
		            <a href="<?php echo $link; ?>">
		                <h4>
		                    <?php echo $title; ?>
		                </h4>
		            </a>
		            <p>
		                <?php echo excerpt(14);?>
		            </p>
		            <div class="blogAction">
					<span><?php the_time('M j, Y'); echo " by "; if($author_name){echo $author_name;}else{the_author();}?></span>
		                <ul class="socialSharePostList list-inline">
		                    <li class="list-inline-item">
		                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="fb-xfbml-parse-ignore">
		                            <i class="fab fa-facebook-square"></i>
		                        </a>
		                    </li>
		                    <li class="list-inline-item">
		                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>&source=<?php echo home_url();?>">
		                            <i class="fab fa-linkedin"></i>
		                        </a>
		                    </li>
		                    <li class="list-inline-item">
		                        <a href="http://twitter.com/home?status=Currentlyreading <?php echo $link; ?>" title="Click to share this post on Twitter">
		                            <i class="fab fa-twitter-square"></i>
		                        </a>
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
	    <!-- blog end -->
		   
		  <? }
		  }
		  $post = $orig_post;
		  wp_reset_query();
		  ?>
	</div>
</section>
<?php endif; ?>

<?php get_footer(); ?>