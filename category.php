<?php 
get_header(); 
$category = get_queried_object();
$catID = $category->term_id;
$catName = $category->slug;
?>

	<main role="main">
		<!-- section -->
		<section class="blogList">

			<h1 class="text-center py-5 homeTitle"><?php single_cat_title(); ?></h1>


			<?php get_template_part('customloopv2'); ?>

		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
		<!-- Cats section -->
		<?php if($catName == "wedding-quizzes"): ?>
		<h3 class="text-center py-3 mt-5 homeTitle">Browse <?php single_cat_title(); ?> by Category</h3>
    	<section class="blogList">
            	<div class="blogContainer">
					<ul class="cat-list">
						<?php 
							$args = array('parent' => $catID);
							$categories = get_categories( $args );
							foreach($categories as $category) { 
								echo '<li class="cat-item"><a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </li> ';
							}?>
					</ul>

				</div>
    	</section>
		<?php endif;?>
	</main>

<?php get_footer(); ?>