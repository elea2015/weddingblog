<?php get_header(); ?>

	<main role="main">
		<?php if(is_home()):?>
			<h1 class="text-center py-3 mt-5 homeTitle">Wedding Hacks From Wedding Hashers</h1>
		<?php endif; ?>
		<div class="blogConteiner">
			<div id="searchFormContainer" class="py-3">
				<?php get_template_part('searchform'); ?>
			</div>
		</div>
		<!-- Recent section -->
		<section class="blogList">
			<?php get_template_part('customloopv2'); ?>
		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
			<?php get_template_part('pagination'); ?>
		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>
