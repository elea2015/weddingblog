<footer>
  <div class="container">
    <div class="footerFlex">
      <div class="mb-3 px-0 footer1">
        <a href="https://weddinghashers.com/"><img id="logo-footer" src="<?php echo get_template_directory_uri(); ?>/img/n-logo.png" alt=""></a>
        <p class="mt-2">Copyright © Wedding Hashers 2021</p>      
      </div>
      <div class="footer2">
        <ul class="list-unstyled">
          <li><a href="https://weddinghashers.com/">Home</a></li>
          <li><a href="https://weddinghashers.com/wedding-hashtags-couples">Couples</a></li>
          <li><a href="https://weddinghashers.com/faq">FAQs</a></li>
          <li><a href="https://weddinghashers.com/wedding-hashtag-generator">Hashtag Generator</a></li>
          <li><a href="https://weddinghashers.com/quizzes">Quizzes</a></li>
        </ul>
      </div>
      <div class="footer3">
        <ul class="list-unstyled">
          <li><a href="/">Blog</a></li>
          <li><a href="https://weddinghashers.com/#contactModal">Contact</a></li>
          <li><a href="https://weddinghashers.com/writers">Writers</a></li>
          <li><a href="https://weddinghashers.com/affiliate">Affiliates</a></li>
        </ul>
      </div>
      <div class="footer4">
        <ul class="list-unstyled">
          <li><a href="https://weddinghashers.com/terms-of-use">Terms of Use</a></li>
          <li><a href="https://weddinghashers.com/privacy-policy">Privacy Policy</a></li>
        </ul>
      </div>
      <div class="footer5">
        <div class="ctaSubscribe ctaSubscribeP">
            <p class="text-center m-0">Need #FreeWeddingTips?</p>
            <p class="text-center">We Can Help!</p>
            <div class="ctaRow">
                <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]')?>
            </div>
        </div>
     
      <!-- social -->
      <div class="socialFooter mt-2 d-none">
        <p>Follow us</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="https://www.instagram.com/kicksta.co/" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/img/Instagramicon.svg" alt="">
            </a>
          </li>
          <li class="list-inline-item">
            <a href="https://www.facebook.com/Kicksta.co/" target="_blank">
              <img src="<?php echo get_template_directory_uri(); ?>/img/FBicon.svg" alt="">
            </a>
          </li>
        </ul>
      </div>
      <!-- social -->
      </div>
    </div>
  </div>
</footer>

<section class="legal text-center py-3">
  <ul class="list-inline">
    <li class="list-inline-item">&copy; <?php echo date('Y');?></li>
    <li class="list-inline-item">
      <a href="https://kicksta.co/terms-of-service">Terms of service</a>
    </li>
  </ul>
</section>

		<?php wp_footer(); ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/infinite.js"></script>
   <!--  <script src="<?php //echo get_template_directory_uri(); ?>/js/myscript.js"></script> -->

<script>
  window.intercomSettings = {
    app_id: "hg32et9j"
  };
</script>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hg32et9j';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<script src="//rum-static.pingdom.net/pa-5b463bfcef13ce0016000164.js" async></script> -->
	</body>
</html>
