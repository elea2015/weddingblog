<footer class="bg-white pt-3 border-top-footer footer-v2">
    <div class="container d-none d-md-block">
        <div class="row">
            <div class="my-2 col-12 col-sm-3">
                <img class="mt-2" src="<?php echo get_template_directory_uri(); ?>/img/n-logo.png" alt="logo-wedding-hashers">
                <p class="mt-3 mb-1 footer--contact"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/map-pin.svg" alt=""> 715 J St, Suite 306, San Diego, CA 92101</p>
                <p class="footer--contact"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/phone.svg" alt=""> <a href="tel:619 259-0443">619 259-0443</a></p>
                <p class="mt-2 footer--copyritht">Copyright © Wedding Hashers <?php the_date('Y'); ?></p>
            </div>
            <div class="my-2 col-12 col-sm-2">
                <ul style="list-style-type:none;" class="p-0">
                    <li class="my-2">
                        <a href="https://weddinghashers.com/" class="text-dark"><u>Home</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/wedding-hashtags-couples" class="text-dark"><u>Couples</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/faq" class="text-dark"><u>FAQ</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/wedding-hashtag-generator" class="text-dark"><u>Hashtag Generator</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/quizzes" class="text-dark"><u>Quizzes</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.honeymoonwishes.com/" target="_blank" class="text-dark"><u>Honeymoon Registry</u></a>
                    </li>
                </ul>
            </div>
            <div class="my-2 col-12 col-sm-2">
                <ul style="list-style-type:none;">
                    <li class="my-2">
                        <a href="/" class="text-dark"><u>Blog</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/contact" class="text-dark"><u>Contact</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/writers" class="text-dark"><u>Writers</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/affiliate" class="text-dark"><u>Affiliates</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/privacy-policy" class="text-dark"><u>Privacy Policy</u></a>
                    </li>
                    <li class="my-2">
                        <a href="https://weddinghashers.com/terms-of-use" class="text-dark"><u>Terms of Use</u></a>
                    </li>
                </ul>
            </div>
            <div class="mt-2 col-12 col-sm-2 mb-4">
                <p class="mt-2">Follow us on social media</p>
                <div>
                    <a class="mr-3" href="https://www.facebook.com/WeddingHashers" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook.svg" src="images/facebook.svg" alt="facebook">
                    </a>
                    <a href="https://www.instagram.com/theweddinghashers/" target="_blank">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/instagram.svg" src="images/instagram.svg" alt="instagram">
                    </a>
                </div>
                <div style="margin-top: 15px;">
                    <!-- TrustBox widget - Mini -->
                    <div class="trustpilot-widget" data-locale="en-US" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="5f4fd699d58f0300019390f8" data-style-height="150px" data-style-width="70%" data-theme="light">
                        <a href="https://www.trustpilot.com/review/weddinghashers.com" target="_blank" rel="noopener">Trustpilot</a>
                    </div>
                    <!-- End TrustBox widget -->
                </div>
            </div>
            <div class="mt-2 col-sm-3 footer--subscribe">
                <p class="mt-2 text-center">Need #FreeWeddingTips?<br>We Can Help!</p>
                <?php echo do_shortcode('[gravityform id=1 title=false description=false ajax=true]')?>
            </div>
        </div>
    </div>
    <div class="d-md-none">
        <div class="d-flex justify-content-center">
            <img src="<?php echo get_template_directory_uri(); ?>/img/n-logo.png" alt="logo-wedding-hashers">
        </div>
        <div class="d-flex justify-content-around my-3">
            <div>
                <ul style="list-style-type:none;">
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Home</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Couples</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>FAQ</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" target="_blank" class="text-dark"><u>Blog</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Writers</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Quizzes</u></a>
                    </li>
                </ul>
            </div>
            <div>
                <ul style="list-style-type:none;">
                    <li class="my-2">
                        <a href="#" target="_blank" class="text-dark"><u>Honeymoon Registry</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Contact</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Affiliates</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Hashtag Generator</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Privacy Policy</u></a>
                    </li>
                    <li class="my-2">
                        <a href="#" class="text-dark"><u>Terms of Use</u></a>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <p class="mt-2 text-center">Follow us on social media</p>
            <div class="d-flex justify-content-center">
                <a class="mr-3" href="https://www.facebook.com/WeddingHashers" target="_blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook.svg" alt="facebook">
                </a>
                <a href="https://www.instagram.com/theweddinghashers/" target="_blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/instagram.svg" alt="instagram">
                </a>
            </div>
            <div style="margin-top: 20px;">
                   <!-- TrustBox widget - Micro Star -->
                <div class="trustpilot-widget" data-locale="en-US" data-template-id="5419b732fbfb950b10de65e5" data-businessunit-id="5f4fd699d58f0300019390f8" data-style-height="24px" data-style-width="100%" data-theme="light">
                <a href="https://www.trustpilot.com/review/weddinghashers.com" target="_blank" rel="noopener">Trustpilot</a>
                </div>
                <!-- End TrustBox widget -->
                <div class="my-3 d-flex w-90 mx-auto my-3 justify-content-center align-items-center">
                    <p class="mx-1"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/phone.svg" alt=""> <a style="font-size: 13px" href="tel:619 259-0443">619 259-0443</a></p>
                    <p style="font-size: 13px" class="mx-1"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/map-pin.svg" alt=""> 715 J St, Suite 306, San Diego, CA 92101</p>
                </div>
            </div>
            <p class="my-2 text-center">Copyright © Wedding Hashers <?php the_date('Y') ?></p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
    <script src="<?php echo get_template_directory_uri(); ?>/js/infinite.js"></script>
   <!--  <script src="<?php //echo get_template_directory_uri(); ?>/js/myscript.js"></script> -->

<script>
  window.intercomSettings = {
    app_id: "hg32et9j"
  };
</script>
<!-- <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/hg32et9j';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
<script src="//rum-static.pingdom.net/pa-5b463bfcef13ce0016000164.js" async></script> -->
	</body>
</html>