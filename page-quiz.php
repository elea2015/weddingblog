<?php /* Template name: Quizzes */ 
get_header();
?>

	<main role="main">
        <h1 class="text-center py-3 mt-5 homeTitle">Wedding Hacks From Wedding Hashers</h1>
		<div class="blogConteiner">
			<div id="searchFormContainer" class="py-3">
				<?php get_template_part('searchform'); ?>
			</div>
		</div>
        <h2 class="text-center py-3 mt-5 homeTitle"><?php the_title(); ?></h2>
		<!-- Quiz section -->
		<section class="blogList">
            <div class="blogContainer quizContainer">
                <?php 
                    // Custom WP query recent
                    $args_quizzes = array(
                        'posts_per_page' => 8,
                        'post_type' => 'quiz',
                        'paged' => get_query_var('paged') ? get_query_var('paged') : 1 
                    );
                    $quizzes = new WP_Query ( $args_quizzes );
                    while ( $quizzes->have_posts() ): $quizzes->the_post(); $a++;
                    //variables
                    $title          = get_the_title();
                    $image          = get_the_post_thumbnail_url('large');
                    $link           = get_the_permalink();
                    $thePostId      = get_the_ID();
                   
                ?>
    
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="blog third">
                                <div class="blogImg">
                                    <a href="<?php echo $link; ?>"><img src="<?php the_post_thumbnail_url('large'); ?>" alt=""></a>
                            </div>
                            <div class="blogBody">
                                <a href="<?php echo $link; ?>"><h3><?php echo $title;?></h3></a>
                                <div class="blogAction">
                                    <span><?php the_time('M j, Y'); echo " by "; //the_author();?></span>
                                    <ul class="socialSharePostList list-inline">
                                        <li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>" class="fb-xfbml-parse-ignore"><i class="fab fa-facebook-square"></i></a></li>
                                        <li class="list-inline-item"><a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>&title=<?php echo $title; ?>&source=<?php echo home_url();?>"><i class="fab fa-linkedin"></i></a></li>
                                        <li class="list-inline-item"><a href="http://twitter.com/home?status=Currentlyreading <?php echo $link; ?>" title="Click to share this post on Twitter"><i class="fab fa-twitter-square"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </article>
                        <!-- start optin card-->
                    <?php if($a == 2):?>
                    <div class="blog third optin">
                        <div class="blogImg">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/wh-cta.png" alt="">
                        </div>
                        <div class="blogBody text-center">
                            <h4>Order Your Custom Hashtags</h4>
                            <!-- <p>We've helped thousands of couples find their perfect wedding hashtag. Enter your details and get yours!</p> -->
                            <div class="ctaRow">
                                <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true]');?>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                <?php 
                endwhile;
                ?>
            </div>
		</section>
		<!-- /section -->
		<section class="blogPagination py-5">
            <?php $big = 999999999; // need an unlikely integer
                echo paginate_links( array('base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),'format' => '?paged=%#%','current' => max( 1, get_query_var('paged') ),'total' => $quizzes->max_num_pages,)); 
            ?>
		</section>
		<!-- /section -->
        <?php wp_reset_postdata();?>
	</main>

<?php get_footer(); ?>
